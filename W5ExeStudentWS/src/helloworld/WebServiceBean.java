package helloworld;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import helloworld.client.WebServiceClient;

@Named(value = "webServiceBean")
@SessionScoped
public class WebServiceBean implements Serializable {
	
	private String username = "";
	
	private WebServiceClient webServiceClient;

	public WebServiceBean() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setWebServiceClient() {
		webServiceClient = new WebServiceClient();
		webServiceClient.setPostName2(this.getUsername());
	}
}